if(process.env.NODE_ENV){
    config = require('./config.'+ process.env.NODE_ENV);
}
else{
    config = require('./config.global');
}

var api = require('./server/api');
var socket = require('./server/socket');

//start the socket service
socket.init(config);

//start the regular api
api.init(config, socket);