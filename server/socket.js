/* jshint node: true */
'use strict';
var WebSocketServer = require('ws').Server;
var socket = {};
var loginMsg = require('../messages/login');
var auth = require('../server/auth');
var Joi = require('joi');
var randtoken = require('rand-token');
var wsConnection = null;

socket.init = function(config){
    console.log('Starting websocket server at port: '+ config.socketPort + '\n');
    var wss = new WebSocketServer({port:config.socketPort});

    var users = [];

    wss.on('connection', function(ws) {
        //for use in other functions
        wsConnection = ws;

        ws.on('message', function(message) {
            try{
                message = JSON.parse(message);
                if(message.type){
                    switch (message.type){
                        case 'login':
                            Joi.validate(message, loginMsg, function (err, value) {
                                if(err){
                                    console.log('Bad login message caused: %s', err);
                                    ws.send('That was disgusting.');
                                }
                                else{
                                    if(message && message.name){
                                        ws.send("Nice to meet you " + message.name);
                                    }
                                }
                            });
                            break;
                        default:
                            console.log('Unrecognized type: %s', message.type);
                            ws.send('You are not my type.');
                    }
                }
                else{
                    console.log('server received message: %s without a type', message);
                    ws.send('I dont know you.');
                }
            }
            catch(e){
                console.log('Badly Formed message caused: '+ e.message);
            }
        });
    });
};

socket.sendPosition = function(position){
    if(wsConnection){
        wsConnection.send(JSON.stringify({type:'position', data:position}));
    }
};

module.exports = socket;
